﻿using UnityEngine;
using System.Collections;

public class Progressbar : MonoBehaviour {

	public float barDisplay; //current progress
	public Vector2 pos = new Vector2(10,10);
	public Vector2 size = new Vector2(60,20);
	public Texture2D emptyTex;
	public Texture2D fullTex;
	public bool displayBar;
	public float speed;


	void OnGUI() {

		if (!displayBar)
			return;
		//draw the background:
		GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
		GUI.Box(new Rect(0,0, size.x, size.y), emptyTex);

		//draw the filled-in part:
		GUI.BeginGroup(new Rect(0,0, size.x * barDisplay, size.y));
		GUI.Box(new Rect(0,0, size.x, size.y), fullTex);
		GUI.EndGroup();
		GUI.EndGroup();
	}

	void Update() {

		pos = Camera.main.WorldToScreenPoint (new Vector3(transform.position.x-.5f, -transform.position.y, 1));

	}

	public bool load(){
		displayBar = true;
		Debug.Log (barDisplay);

		barDisplay += Time.deltaTime*speed;
		if (barDisplay < 1)
			return false;
		else
			return true;
	}

	public void startTimer(){
		barDisplay = 0;
		displayBar = false;
	}

}
