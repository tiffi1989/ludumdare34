﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class mutantGrow : growPot {


	private bool mutant;
	public GameObject player;
	public GameObject bulletPrefab;
	public int hitCounter;
	private GameObject[] canvas;
	private AudioSource shroom;


	void Start () {
		dope = GameObject.Find ("dope").GetComponent<AudioSource> ();

		shroom = GameObject.Find ("shroom").GetComponent<AudioSource>();
		text = GameObject.Find("Score").GetComponent<AddMoney>();
		rendererParent = GetComponent<SpriteRenderer> ();
		boxCollider = GetComponent<BoxCollider2D> ();
		boxCollider.enabled = false;
		circleCollider = GetComponent<CircleCollider2D> ();
		buyStuff = Camera.main.GetComponent<BuyStuff> ();
		canvas = Camera.main.GetComponent<PoliceSpwaner> ().canvas;
		player = GameObject.Find ("Player");
	}



	void Update () {

		if (!isPlaced) {
			PlacePot ();
			return;
		}

		if(cSeed != null)
			time = time + Time.deltaTime;

		if (time > timeToGrow / potSpeedMultiplicator && !isGrown && !mutant)
			grow ();

		if (mutant) {
			Vector3 vel = new Vector3 (player.transform.position.x  - transform.position.x, player.transform.position.y - transform.position.y -1, 0f);
			time += Time.deltaTime;
			if (time > .5f) {
				shroom.Play ();
				(Instantiate (bulletPrefab, new Vector3 (transform.position.x, transform.position.y + 1, 0), Quaternion.identity) as GameObject).GetComponent<Rigidbody2D> ().velocity = vel;
				time = 0;
			}

			if (hitCounter > 2) {
				killMutant ();
			}
				
		}


	}



	void grow(){
		Transform theSeed = transform.Find ("seed");
		theSeed.gameObject.SetActive (false);
		float rnd = Random.Range (1, 4);
		if (rnd == 1) {
			Transform toGrow = transform.Find ("mutantShroom");
			toGrow.gameObject.SetActive (true);
			mutant = true;
			boxCollider.size = new Vector2 (1f, 3.8f);
			boxCollider.offset = new Vector2 (0, 1.3f);
			player.GetComponent<PlayerControls> ().setFight (true);
			foreach(GameObject o in canvas)
				o.SetActive(false);
		} else {
			Transform toGrow = transform.Find (cSeed);
			toGrow.gameObject.SetActive (true);
			isGrown = true;
		}



	}

	private void killMutant(){
		hitCounter = 0;
		foreach(GameObject render in rendererChild)
			render.SetActive(false);
		mutant = false;
		isGrown = false;

		boxCollider.size = new Vector2 (1f, 1.13f);
		boxCollider.offset = new Vector2 (0, 0f);

		cSeed = null;

		player.GetComponent<PlayerControls> ().setFight (false);
		foreach(GameObject o in canvas)
			o.SetActive(true);
	}


	void OnTriggerEnter2D(Collider2D other){
		if (!isPlaced) {
			placeAble = false;
			close.SetActive (true);
		}
	}




}
