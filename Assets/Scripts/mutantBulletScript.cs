﻿using UnityEngine;
using System.Collections;

public class mutantBulletScript : MonoBehaviour {

	private SpriteRenderer rendererS;
	private ParticleSystem particle;

	// Use this for initialization
	void Start () {
		rendererS = GetComponent<SpriteRenderer> ();
		particle = GetComponentInChildren<ParticleSystem> ();
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.CompareTag ("Player")) {
			particle.Play ();
			rendererS.enabled = false;
			col.gameObject.GetComponent<PlayerControls> ().lifes -= 1;
			Invoke ("destroyMe", 1);
		}
	}

	void destroyMe(){
		Destroy (gameObject);
	}
}
