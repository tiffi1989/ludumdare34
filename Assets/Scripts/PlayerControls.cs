﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerControls : MonoBehaviour {

	public float speed;
	private Rigidbody2D rig;
	private CircleCollider2D circle;
	ArrayList thingsInRange = new ArrayList ();
	Animator ani;
	private Progressbar progress;
	public bool doneSmth, fighting;
	public BuyStuff buyStuff;
	public Meldungen messages;
	private ParticleSystem particle;
	public GameObject bulletPrefab;
	float time;
	public int lifes;
	private ParticleSystem deathP;
	bool dead;
	public Slider healthBar;
	public AudioSource au;
	public AudioSource tot;
	public AudioSource shroom;

	// Use this for initialization
	void Start () {
		rig = GetComponent<Rigidbody2D> ();
		circle = GetComponent<CircleCollider2D> ();
		ani = GetComponent<Animator> ();
		progress = GetComponent<Progressbar> ();
		particle = GetComponentInChildren<ParticleSystem> ();
		deathP = GameObject.Find ("Death").GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		movement ();

		Debug.Log (fighting);

		healthBar.value = lifes;


		if (Input.GetKey(KeyCode.F) && !doneSmth) {
			if (thingsInRange.Count > 0) {

				growPot tmp = thingsInRange [0] as growPot;

				if (!tmp.isInteractAble ()) {					
					messages.SetText ("Grow grow grow your boat!!");
					return;
				}
				if (!tmp.isEmpty () || (tmp.isEmpty () && buyStuff.checkSeeds ()) ) {

					if (progress.load ()) {
					
						tmp.interact ();
						progress.startTimer ();
						doneSmth = true;

				}}else if(!buyStuff.checkSeeds()){
					messages.SetText ("Not enough seeds!");
				}
			}
		}

		if (Input.GetKeyUp (KeyCode.F))
			doneSmth = false;

		if (Input.GetAxis ("Fire1") > 0 && fighting) {
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			Vector3 vel = new Vector3 (mousePosition.x  - transform.position.x, mousePosition.y - transform.position.y, 0f);
			time += Time.deltaTime;
			if (time > .3f) {
				GameObject tmp = Instantiate (bulletPrefab, new Vector3 (transform.position.x, transform.position.y, 0), Quaternion.identity) as GameObject;
				tmp.GetComponent<Rigidbody2D> ().angularVelocity = 400f;
				tmp.GetComponent<Rigidbody2D> ().velocity = vel * 1.5f;
				time = 0;
				shroom.Play ();
			}
		}

		if (lifes <= 0 && !dead) {
			dead = true;
			GetComponent<SpriteRenderer> ().enabled = false;
			deathP.Play();
			tot.Play ();
			Invoke ("loadAgain", 3);
		}
	}

	void loadAgain(){
		Application.LoadLevel (0);
	}
		
	void movement(){
		ani.speed = 1;
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		if (moveVertical != 0) {

			if (moveVertical < 0) {
				ani.SetBool ("Up", true);
				ani.SetBool ("Down", false);
				ani.SetBool ("Idle", false);
				ani.SetBool ("Side", false);
				circle.offset = new Vector2 (0, -1f);

			} else {
				ani.SetBool ("Up", false);
				ani.SetBool ("Down", true);
				ani.SetBool ("Idle", false);
				ani.SetBool ("Side", false);
				circle.offset = new Vector2 (0, .65f);

			}				
		} else if (moveHorizontal != 0) {
			if (moveHorizontal < 0) {
				transform.localScale = new Vector3 (-.8f, .8f, 1f);
			} else {
				transform.localScale = new Vector3 (.8f, .8f, 1f);
				}
			circle.offset = new Vector2 (.6f, -.2f);

			ani.SetBool ("Up", false);
			ani.SetBool ("Down", false);
			ani.SetBool ("Idle", false);
			ani.SetBool ("Side", true);
		} else {
			ani.SetBool ("Up", false);
			ani.SetBool ("Down", false);
			ani.SetBool ("Idle", true);
			ani.SetBool ("Side", false);
		}
		Vector3 movement = new Vector3 (moveHorizontal, moveVertical, 0f);

		if (moveHorizontal == 0 && moveVertical == 0)
			ani.speed = 0;

		rig.velocity = movement * speed;
	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.GetComponentInChildren<growPot>()!= null)
			thingsInRange.Add (col.GetComponentInChildren<growPot>());
		if(col.gameObject.CompareTag("Bullet") && !dead){
			if(!particle.isPlaying)
			particle.Play();
			Destroy (col.gameObject);
			lifes -= 1;
			au.Play ();
		}
	}


	void OnTriggerExit2D(Collider2D col){
		if (col.GetComponentInChildren<growPot> () != null) {
			thingsInRange.Clear();
		}
	}

	public void setFight(bool fight){
		fighting = fight;
		thingsInRange.Clear();
	}


}
