﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Meldungen : MonoBehaviour {

	private Text text;
	public bool shouldFadeOut;
	private float fading = 1;
	public Text text2;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
	}
	
	void Update () {
		if (shouldFadeOut) {
			fading -= 0.005f;
			text.color = new Color (0, 0, 0, fading);
			text2.color = new Color (0, 0, 0, fading);


		}

		if (text.color.a <= 0)
			shouldFadeOut = false;
	}

	public void SetText(string message){
		text.text = message;
		text2.text = message;
		fading = 1;
		text.color = new Color (0, 0, 0, fading);
		text2.color = new Color (0, 0, 0, fading);

		CancelInvoke ("setShouldFadeOut");
		Invoke ("setShouldFadeOut" , 4.5f);
	}

	private void setShouldFadeOut(){
		shouldFadeOut = true;
	}


}
