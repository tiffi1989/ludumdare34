﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PoliceSpwaner : MonoBehaviour {
	public GameObject prefabPolice;
	public GameObject black;
	public GameObject[] canvas;
	public PlayerControls player;
	private Enemy[] enemy = new Enemy[4];
	private AddMoney score;
	int lastTimeSpawned;
	public Meldungen meldungen;
	public Text text2;
	bool trigger;

	public AudioSource sound;
	void Start(){
		score = GameObject.Find("Score").GetComponent<AddMoney>();
	}


	void Update () {



		bool allDead = true;
		foreach (Enemy e in enemy) {
			allDead &= e == null;
		}

		if (player.fighting && allDead && trigger) {
			player.setFight (false);
			foreach(GameObject o in canvas)
				o.SetActive(true);
			trigger = false;
		}

		if (score.money > 100 && lastTimeSpawned < 100) 
			spawn ();
		if (score.money > 200 && lastTimeSpawned < 200) 
			spawn ();
		if (score.money > 500 && lastTimeSpawned < 500) 
			spawn ();
		if (score.money > 800 && lastTimeSpawned < 800) 
			spawn ();
		if (score.money > 1000 && lastTimeSpawned < 1000) 
			spawn ();
		if (score.money > 2000 && lastTimeSpawned < 2000) 
			spawn ();
		if (score.money > 3000 && lastTimeSpawned < 3000) 
			spawn ();
		if (score.money > 4000 && lastTimeSpawned < 4000) 
			spawn ();
		if (score.money > 5000 && lastTimeSpawned < 5000) 
			spawn ();
		if (score.money > 6000 && lastTimeSpawned < 6000) 
			spawn ();


		
		

						

	}

	private void spawn(){
		lastTimeSpawned = score.money;
		setBlack ();
		Invoke ("spawnPolice", 2);
		Invoke ("setWhite", 2);
	}

	private void spawnPolice(){

		meldungen.gameObject.SetActive (true);
		text2.gameObject.SetActive (true);

		trigger = true;

		meldungen.SetText ("Fuck, the Police, Left click to dope them!");
		enemy[0] = (Instantiate (prefabPolice, new Vector3 (-7, 3, 0), Quaternion.identity)as GameObject).GetComponent<Enemy>();
		if(score.money > 300)
		enemy[1] = (Instantiate (prefabPolice, new Vector3 (7, 3, 0), Quaternion.identity)as GameObject).GetComponent<Enemy>();
		if(score.money > 1000)
		enemy[2] = (Instantiate (prefabPolice, new Vector3 (-7, -3, 0), Quaternion.identity)as GameObject).GetComponent<Enemy>();
		if(score.money > 4000)
		enemy[3] = (Instantiate (prefabPolice, new Vector3 (7, -3, 0), Quaternion.identity)as GameObject).GetComponent<Enemy>();
		foreach(GameObject o in canvas)
			o.SetActive(false);
		player.setFight (true);

	}

	private void setBlack(){
		black.SetActive (true);
		sound.Play ();

	}

	private void setWhite(){
		black.SetActive (false);

	}

}
