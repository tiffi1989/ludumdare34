﻿using UnityEngine;
using System.Collections;

public class diceTopfScript : growPot {

	void Update () {

		//Debug.Log ("Current Seed: " + cSeed + " time: " +  time + " timeToGrow " + timeToGrow);
		if (!isPlaced) {
			PlacePot ();
			return;
		}

		if(cSeed != null)
			time = time + Time.deltaTime;

		if (time > timeToGrow / potSpeedMultiplicator && !isGrown)
			grow ();



	}

	void grow(){
		Transform theSeed = transform.Find ("seed");
		theSeed.gameObject.SetActive (false);

		int rnd = Random.Range (1, 6);

		Debug.Log (rnd);

		switch(rnd){
		case 1: cSeed = "pot";
			break;
		case 2: cSeed = "coca";
			break;
		case 3:
			cSeed = "opium";
			break;
		case 4:
			cSeed = "shroom";
			break;
		case 5:
			cSeed = "pill";
			break;
		default:
			break;

		}


		Transform toGrow = transform.Find (cSeed);
		toGrow.gameObject.SetActive (true);

		isGrown = true;
	}

	void setSeeded(){
		cSeed = buyStuff.currentSeed;
		Transform theSeed = transform.Find ("seed");
		theSeed.gameObject.SetActive (true);
		timeToGrow = Random.Range (2 , 40);
		buyStuff.removeSeed ();
	}
}
