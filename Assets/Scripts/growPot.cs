﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class growPot : MonoBehaviour {
	
	protected float time;
	public float timeToGrow;
	public GameObject close;
	public GameObject[] rendererChild;
	protected AddMoney text;
	protected bool isPlaced, isGrown;
	protected bool placeAble = true;
	protected SpriteRenderer rendererParent;
	protected BoxCollider2D boxCollider;
	protected CircleCollider2D circleCollider;
	protected BuyStuff buyStuff;
	protected string cSeed = null;
	public int potMoneyMultiplicator;
	public int potSpeedMultiplicator;
	public int wert;
	public AudioSource dope;



	// Use this for initialization
	void Start () {
		dope = GameObject.Find ("dope").GetComponent<AudioSource> ();
		text = GameObject.Find("Score").GetComponent<AddMoney>();
		rendererParent = GetComponent<SpriteRenderer> ();
		boxCollider = GetComponent<BoxCollider2D> ();
		boxCollider.enabled = false;
		circleCollider = GetComponent<CircleCollider2D> ();
		buyStuff = Camera.main.GetComponent<BuyStuff> ();

	}
	
	// Update is called once per frame
	void Update () {

		//Debug.Log ("Current Seed: " + cSeed + " time: " +  time + " timeToGrow " + timeToGrow);
		if (!isPlaced) {
			PlacePot ();
			return;
		}

		if(cSeed != null)
			time = time + Time.deltaTime;

		if (time > timeToGrow / potSpeedMultiplicator && !isGrown)
			grow ();



	}

	void setSeeded(){
		cSeed = buyStuff.currentSeed;
		Transform theSeed = transform.Find ("seed");
		theSeed.gameObject.SetActive (true);
		switch (cSeed) {
		case "pot":
			timeToGrow = 5;
			break;
		case "coca":
			timeToGrow = 8;			
			break;
		case "opium":
			timeToGrow = 15;			
			break;
		case "shroom":
			timeToGrow = 25;			
			break;
		case "pill":
			timeToGrow = 40;			
			break;
		default:
			break;

		}
		buyStuff.removeSeed ();


	}


	void grow(){
		Transform theSeed = transform.Find ("seed");
		theSeed.gameObject.SetActive (false);
		Transform toGrow = transform.Find (cSeed);
		toGrow.gameObject.SetActive (true);

		isGrown = true;
	}

	protected void PlacePot(){
		Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mousePosition.z = 0;
		rendererParent.color = new Color (1f, 1f, 1f, 0.5f);

		transform.position = mousePosition;

		if (Input.GetKeyUp (KeyCode.Mouse0) && placeAble) {
			isPlaced = true;
			boxCollider.enabled = true;
			circleCollider.enabled = false;
			rendererParent.color = new Color (1, 1, 1, 1f);
		}

	

	} 

	public void HarvestPot(){
		if (!isGrown)
			return;
		time = 0;
		foreach(GameObject render in rendererChild)
			render.SetActive(false);

		switch (cSeed) {
		case "pot":
			text.Add (3 * potMoneyMultiplicator);
			break;
		case "coca":
			text.Add (10 * potMoneyMultiplicator);
			break;
		case "opium":
			text.Add (20 * potMoneyMultiplicator);
			break;
		case "shroom":
			text.Add (40 * potMoneyMultiplicator);
			break;
		case "pill":
			text.Add (200 * potMoneyMultiplicator);
			break;
		default:
			break;

		}

		dope.Play ();

		isGrown = false;

		cSeed = null;
		


	}




	void OnTriggerEnter2D(Collider2D other){
		if (!isPlaced) {
			placeAble = false;
			close.SetActive (true);
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (!isPlaced) {
			placeAble = true;
			close.SetActive (false);
		}
	}

	public void interact(){
		if (cSeed == null && !isGrown)
			setSeeded ();
		else if(isGrown)
			HarvestPot();
	}

	public bool isInteractAble(){
		if (isGrown || cSeed == null)
			return true;
		else
			return false;
	}

	public bool isEmpty(){
		if (cSeed == null && !isGrown)
			return true;
		else if (isGrown)
			return false;

		return false;
		}




}
