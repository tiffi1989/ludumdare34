﻿using UnityEngine;
using System.Collections;

public class uranGrow : growPot {

	void Start () {
		dope = GameObject.Find ("dope").GetComponent<AudioSource> ();

		text = GameObject.Find("Score").GetComponent<AddMoney>();
		rendererParent = GetComponent<SpriteRenderer> ();
		boxCollider = GetComponent<BoxCollider2D> ();
		boxCollider.enabled = false;
		circleCollider = GetComponent<CircleCollider2D> ();
		buyStuff = Camera.main.GetComponent<BuyStuff> ();
		GetComponentInChildren<ParticleSystem> ().Play ();

	}

}
