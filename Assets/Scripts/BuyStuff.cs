﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuyStuff : MonoBehaviour {

	public GameObject shop;
	public Object prefabTopf, prefabSell, prefabGoldTopf, prefabBigTopf, prefabUranTopf, prefabDiceTopf, prefabJamaicaTopf,prefabMutantTopf;
	public GameObject messages;
	public GameObject hat;
	private AddMoney text;
	public Text[] Seeds;
	//0 Pot, 1 coca, 2 shroom, 3 opium, 4 pill
	private int[] seedAmount = new int[] {1,0,0,0,0};
	private string[] seedName = new string[] {"Pot","Coca", "Opium","Shroom", "Special"};
	public string currentSeed;
	public int[] seedCost;
	public bool sellMode;
	private GameObject sellMouse;
	public Button hatButton;
	public GameObject blaster;
	private int healCost = 50;
	public Text healText;
	public PlayerControls player;
	public AudioSource music;
	public Text backgroundtext;

	void Start(){
		text = GameObject.Find("Score").GetComponent<AddMoney>();
		currentSeed = "pot";
		if (Screen.fullScreen = true)
			Screen.fullScreen = false;
	}

	void Update(){
		for (int i = 0; i < Seeds.Length; i++) {
			Seeds [i].text = seedName [i] + " Seeds x" + seedAmount [i];
		}
			

		if (sellMode) {
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			mousePosition.z = 0;

			sellMouse.transform.position = mousePosition;

			if (Input.GetKeyUp (KeyCode.Mouse0)) {
				GameObject potToSell = sellMouse.GetComponent<sell>().collision;
				if (potToSell != null && potToSell.CompareTag ("Topf")) {					
					float tmp = potToSell.GetComponent<growPot> ().wert * 0.75f;
					potToSell.SetActive(false);
					text.Add ((int)tmp);
					endSell ();
				}
			}
		}


	}



	public void BuyTopf(int cost){
		messages.gameObject.SetActive (true);
		backgroundtext.gameObject.SetActive (true);


		if (text.money < cost) {
			messages.GetComponent<Meldungen>().SetText ("You have not enough money!");
			CloseShop ();
			return;
		}

		CloseShop ();


		switch(cost){
		case 10:	Instantiate (prefabTopf, new Vector3 (0, 0, 0), Quaternion.identity);
			messages.GetComponent<Meldungen>().SetText ("Dope Dope Dope.");
			break;
		case 20:	Instantiate (prefabBigTopf, new Vector3 (0, 0, 0), Quaternion.identity);
			messages.GetComponent<Meldungen>().SetText ("YEAHH, two flowers, one seed!");
			break;
		case 50:Instantiate (prefabDiceTopf, new Vector3 (0, 0, 0), Quaternion.identity);
			messages.GetComponent<Meldungen>().SetText ("It's like a slotmashine for Drugs!!!");
			break;
		case 100:Instantiate (prefabJamaicaTopf, new Vector3 (0, 0, 0), Quaternion.identity);
			messages.GetComponent<Meldungen>().SetText ("Hm...The handbook says it just grows weed, but the best WEED in the World!");
			break;
		case 200:Instantiate (prefabGoldTopf, new Vector3 (0, 0, 0), Quaternion.identity);
			messages.GetComponent<Meldungen>().SetText ("Look at that shiny thing. I bet I can sell golden Drugs for like 4 times as much!");
			break;
		case 1000:Instantiate (prefabMutantTopf, new Vector3 (0, 0, 0), Quaternion.identity);
			messages.GetComponent<Meldungen>().SetText ("The handbook just says: 'grows perfectly fine weed... most of the time!'");
			break;
		case 2000:Instantiate (prefabUranTopf, new Vector3 (0, 0, 0), Quaternion.identity);
			messages.GetComponent<Meldungen>().SetText ("Welcome to the 21st century, bigger crops, more money!");
			break;
		}
		text.money = text.money - cost;
	}

	public void BuyHeal(){
		messages.gameObject.SetActive (true);
		backgroundtext.gameObject.SetActive (true);


		if (text.money < healCost) {
			messages.GetComponent<Meldungen>().SetText ("You have not enough money!");
			return;
		}

		messages.GetComponent<Meldungen>().SetText ("What is in that grog stuff anyway??");
		player.lifes = 10;
		CloseShop ();
		text.money = text.money - healCost;
		healCost *= 2;
		healText.text = healCost + " $";
	}




	public void BuyHat(){
		messages.gameObject.SetActive (true);
		backgroundtext.gameObject.SetActive (true);


		if (text.money < 300) {
			messages.GetComponent<Meldungen>().SetText ("You have not enough money!");
			return;
		}

		messages.GetComponent<Meldungen>().SetText ("I'm soooo FABULOUS.");

		CloseShop ();
		hat.SetActive (true);
		text.money = text.money - 10;
	}

	public void BuyBlaster(){
		messages.gameObject.SetActive (true);
		backgroundtext.gameObject.SetActive (true);


		if (text.money < 100) {
			messages.GetComponent<Meldungen>().SetText ("You have not enough money!");
			return;
		}

		messages.GetComponent<Meldungen>().SetText ("uh uh yeahhh!");

		blaster.GetComponent<SpriteRenderer> ().enabled = true;
		blaster.GetComponentInChildren<ParticleSystem> ().Play ();

		music.Play ();

		CloseShop ();
		text.money = text.money - 100;
	}

	public void CloseShop(){
		shop.SetActive (false);
	}

	public void addSeed(int seed){
		if (text.money < seedCost [seed]) {
			messages.GetComponent<Meldungen>().SetText ("You have not enough money!");
			return;
		}

		seedAmount [seed] += 1;
		text.money -= seedCost [seed];

	}

	public void setCurrentSeed(string tmp){
		currentSeed = tmp;
	}

	private void endSell(){
		sellMode = false;
		sellMouse.SetActive (false);
	}

	public void sellPots(){
		if (sellMode) {
			endSell ();
			return;
		}
			
		sellMode = true;
		sellMouse = Instantiate (prefabSell, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
	}

	public bool checkSeeds(){
		switch (currentSeed) {
		case "pot":
			if (seedAmount [0] > 0)
				return true;
			break;
		case "coca":
			if (seedAmount [1] > 0)
				return true;
			break;
		case "opium":
			if (seedAmount [2] > 0)
				return true;
			break;
		case "shroom":
			if (seedAmount [3] > 0)
				return true;
			break;
		case "pill":
			if (seedAmount [4] > 0)
				return true;
			break;
		default:
			break;

		}
		return false;
	}

	public void removeSeed(){
		switch (currentSeed) {
		case "pot":
			seedAmount [0] -= 1;
			break;
		case "coca":
			seedAmount [1] -= 1;			
			break;
		case "opium":
			seedAmount [2] -= 1;			
			break;
		case "shroom":
			seedAmount [3] -= 1;		
			break;
		case "pill":
			seedAmount [4] -= 1;		
			break;
		default:
			break;

		}
	}
		

}
