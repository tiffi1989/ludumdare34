﻿using UnityEngine;
using System.Collections;

public class jamaicaGrow : growPot {

	void Update () {

		//Debug.Log ("Current Seed: " + cSeed + " time: " +  time + " timeToGrow " + timeToGrow);
		if (!isPlaced) {
			PlacePot ();
			return;
		}

		if(cSeed != null)
			time = time + Time.deltaTime;

		if (time > timeToGrow / potSpeedMultiplicator && !isGrown)
			grow ();



	}

	void grow(){
		Transform theSeed = transform.Find ("seed");
		theSeed.gameObject.SetActive (false);

		cSeed = "pot";

		Transform toGrow = transform.Find (cSeed);
		toGrow.gameObject.SetActive (true);

		isGrown = true;
	}

	void setSeeded(){
		cSeed = buyStuff.currentSeed;
		Transform theSeed = transform.Find ("seed");
		theSeed.gameObject.SetActive (true);
		timeToGrow = Random.Range (2 , 40);
		buyStuff.removeSeed ();
	}
}
