﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public Object bulletPrefab;
	private GameObject player;
	private float time;
	private float offset = .5f;
	private int hitCounter;
	private GameObject peng;

	// Use this for initialization
	void Start () {
		peng = GameObject.Find ("peng");
		player = GameObject.Find ("Player");
		if (transform.position.x > 0) {
			transform.localScale = new Vector3 (-1, 1, 1);
			offset = -.5f;
		}
			
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 vel = new Vector3 (player.transform.position.x -offset - transform.position.x, player.transform.position.y - transform.position.y, 0f);
		time += Time.deltaTime;
		if (time > .5f) {
			peng.GetComponent<AudioSource> ().Play ();
			(Instantiate (bulletPrefab, new Vector3 (transform.position.x + offset, transform.position.y, 0), Quaternion.identity) as GameObject).GetComponent<Rigidbody2D> ().velocity = vel;
			time = 0;
		}

		if (hitCounter > 2)
			Destroy (gameObject);
	}

	public void takeHit(){
		hitCounter++;
	}
}
