﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AddMoney : MonoBehaviour {

	private Text text;
	public int money;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
		Add (25);
	}

	void Update(){
		text.text = "$$$: " + money;
	}
	
	public void Add(int amount){
		money += amount;

	}

}
