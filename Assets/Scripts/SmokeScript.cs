﻿using UnityEngine;
using System.Collections;

public class SmokeScript : MonoBehaviour {

	Animator ani;
	Transform trans;

	// Use this for initialization
	void Start () {
		ani = GetComponentInParent<Animator> ();
		trans = GetComponent<Transform> ();
	}

	// Update is called once per frame
	void Update () {
		if (ani.GetCurrentAnimatorStateInfo (0).IsName ("WalkingBack"))
			trans.localPosition = new Vector3 (-0.27f, 0.45f, 0f);
		else
			trans.localPosition = new Vector3 (0.27f, 0.45f, 0f);	

	}
}
